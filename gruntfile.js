module.exports = function(grunt){
    require('time-grunt')(grunt)
    require('jit-grunt')(grunt, function)
    useminPrepare: 'grunt-usemin'
});
grunt.initconfig({
    sass: {
        dist:{
            files:[{
                expand: true,
                cwd: 'css',
                src:['*.scss'],
                dest: 'css',
                ext:'.css'
            }]
        }
    },
    watch:{
        files: ['css/*.scss'],
        Tasks: ['css']
    },
    browserSync: {
        dev:{
            bsfiles{
                //browser files
                src: [
                    'css/*.css',
                    '*.html',
                    'js/*.js'
                ]
            },
            options: {
                watchTask: true,
                server:{
                    baseDir: './' //Directorio base para nuestro servidor
                }
            }
        }
    },
    imagemin: {
       dynamic: {
            files: [{
                expand: true,
                cwd: 'src/',
                src: ['**/*.{png,jpg,gif}'],
                dest: 'dist/'
            }]
        }
         
     },
     copy:{
         html:{
             files[{
                 expanded: true,
                 dot:true,
                 cwd:'./',
                 src:['*.html'],
                 dest: 'dist'
             }]
         },
     },
     clean: {
         build:{
             src['dist/']
         }
     },
     cssmin: {
         dist:{}
     },
     uglify: {
         dist: {}
     },
     filerev: {
         options: {
             enconding: 'utf8',
             algorithm: 'md5',
             length: 20
         },
     },
     release: {
         // filerev:release hashes(md5) all assets (images, js and css)
         // in dist directory
         files:[{
             src:[
                 'dist/js/*.js',
                 'dist/css/*.css',

             ]
         }]
     }
    },
    concat: {
        options: {
            separator:';'
        },
        dist: {}
    },
    useminPrepare: {
        foo:{
            dest: 'dist',
            src: ['index.html', 'about.html', 'precios.html', 'contacto.html']
        },
    options: {
        flow: {
            steps: {
                css:['cssmin'],
                js:['uglify']
            },
post: {
    css: [{
     name: 'cssmin',
     createConfig: function(context, block){
         var generated = context.options.generated;
         generated.options = {
             keepSpecialComments: 0,
             rebase: false
         }
     }   
    }]
}
        }
    }
},
usemin:{
    html: ['dist/index.html', 'dist/precios.html', 'dist/contacto.html'],
    assetsDir: ['dist', 'dist/css', 'dist/js']
 


grunt.registerTasks('css',['sass']);
grunt.registerTasks('default', ['browserSync', 'watch'])
grunt.registerTask('img:compress', ['imagemin']);
grunt.registerTask('build',[
    'clean',
    'copy',
    'imagemin',
    'usemin¨Prepare',
    'concat',
    'cssmin',
    'uglify',
    'filerev',
    'usemin'
])
});
